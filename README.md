# eslint-config-bmcallis

ESLint rule configuration for projects that I work on.
http://eslint.org/docs/rules/

Plugins Used:
- [eslint-plugin-import](https://github.com/benmosher/eslint-plugin-import)
- [eslint-plugin-babel](https://github.com/babel/eslint-plugin-babel)
- [eslint-plugin-react](https://github.com/yannickcr/eslint-plugin-react)

## Usage

Install the configs by running:

```
npm install --save-dev eslint eslint-config-bmcallis
```

Note that the plugins and parser used are dependencies of this project and will also be installed to your project, so you don't need to specify them individually.

Then add the extends to your `.eslintrc.js`:

```javascript
module.exports = {
  extends: ['bmcallis'],
  rules: {
      // your overrides
  }
}
```

## Smaller Bits

By extending `bmcallis` you're getting all of the configs for eslint, import, babel and react. If you only want some of those you can extend them individually.

```javascript
module.exports = {
  extends: [
   'bmcallis/core', // all of the config options built into eslint
   'bmcallis/import',
   'bmcallis/babel',
   'bmcallis/react',
  ],
  rules: {
    // overrides
  }
}
```

A shoutout to [@kentcdodds](https://twitter.com/kentcdodds), a lot of this repo was based off of the work that he did with [eslint-config-kentcdodds](https://github.com/kentcdodds/eslint-config-kentcdodds)
