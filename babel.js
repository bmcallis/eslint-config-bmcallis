// https://github.com/babel/eslint-plugin-babel/tree/master/rules

module.exports = {
  parser: 'babel-eslint',
  env: {
    es6: true,
  },
  parserOptions: {
    sourceType: 'module',
    ecmaVersion: 9,
    ecmaFeatures: {
      jsx: true,
    },
  },
  plugins: [
    'babel',
  ],
  rules: {
    'babel/array-bracket-spacing': 'off', // deprecated
    'babel/arrow-parens': 'off', // deprecated
    'babel/flow-object-type': 'off', // deprecated
    'babel/func-params-comma-dangle': 'off', // deprecated
    'babel/generator-star-spacing': 'off', // deprecated
    'babel/new-cap': [
      'error',
      {
        newIsCap: true,
        capIsNew: true,
      },
    ],
    'babel/no-await-in-loop': 'off', // deprecated
    'babel/no-invalid-this': 'error',
    'babel/no-unused-expressions': 'off',
    'babel/object-curly-spacing': [
      'error',
      'always',
      {
        objectsInObjects: false,
      },
    ],
    'babel/object-shorthand': 'off', // deprecated
    'babel/quotes': 'off',
    'babel/semi': 'off',
    'babel/valid-typeof': 'off',
  },
};
