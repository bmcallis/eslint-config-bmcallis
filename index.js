module.exports = {
  env: {
    browser: true,
    node: true,
  },
  extends: [
    './core.js',
    './babel.js',
    './import.js',
    './react.js',
  ],
  rules: {
  },
}
